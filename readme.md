A PRTG-Sensor for the [Intellinet PDU](https://intellinetnetwork.de/collections/zubehor/products/intellinet-de-19-8-fach-ip-steckdosenleiste-smart-pdu-mit-c13-kaltgeratesteckdosen-163682) or similar devices with this kind of status page:

http://hostname/status.xml

Response:
```<response>
<cur0>0.2</cur0>
<stat0>normal</stat0>
<curBan>0.2</curBan>
<tempBan>24</tempBan>
<humBan>42</humBan>
<statBan>normal</statBan>
<outletStat0>on</outletStat0>
<outletStat1>on</outletStat1>
<outletStat2>on</outletStat2>
<outletStat3>off</outletStat3>
<outletStat4>off</outletStat4>
<outletStat5>off</outletStat5>
<outletStat6>off</outletStat6>
<outletStat7>off</outletStat7>
<userVerifyRes>0</userVerifyRes>
</response>```

Change debug_mode to True if you want to test it locally. 
Make sure that you add and transmit the windows credentials to your sensor. Also add the IP to the "additional parameters" field without http://