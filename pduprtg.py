# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import json
import urllib3
import requests
import sys

from requests.auth import HTTPBasicAuth
from xml.etree import ElementTree

debug_mode = False

# map on/off to 1/0 for PRTG integer values
outlet_map = {
    "on": 1,
    "off": 0
}


def main():
    if not debug_mode:
        prtg = json.loads(sys.argv[1])
        host = prtg['params'].replace("\\", "")
        username = "{}".format(prtg['windowsloginusername'])
        password = prtg['windowsloginpassword']
    else:
        import debug_conf
        host = debug_conf.host
        username = debug_conf.username
        password = debug_conf.password

    # connect to the PDU and read the status.xml
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)  # Disable the SSL Certificate check
    auth = HTTPBasicAuth(username, password)
    status_url = "http://{}/status.xml".format(host)
    response = requests.get(url=status_url, auth=auth)
    response_tree = ElementTree.fromstring(response.content)

    # if the status is not normal, set it to 0
    status = 1
    if "normal" not in response_tree[1].text:
        status = 0

    # create the sensor values
    sensor_values = {
        "prtg": {
            "result": [
                {
                    "channel": "Ampere",
                    "unit": "Custom",
                    "customunit": "A",
                    "float": 1,
                    "value": response_tree[0].text
                },
                {
                    # if the status is 0, set it to an error state in PRTG
                    "channel": "Status",
                    "LimitMode": 1,
                    "LimitMinError": 1,
                    "float": 0,
                    "value": status
                },
                {
                    "channel": "Temperature",
                    "unit": "Temperature",
                    "float": 0,
                    "value": response_tree[3].text
                },
                {
                    "channel": "Humidity",
                    "unit": "Percent",
                    "float": 0,
                    "value": response_tree[4].text
                },
            ]
        }
    }

    # create the output for every plug
    for stat in response_tree:
        if "outletStat" in stat.tag:
            sensor_values['prtg']['result'].append(
                {
                    "channel": stat.tag,
                    "float": 0,
                    "value": outlet_map[stat.text]
                }
            )

    return json.dumps(sensor_values)


if __name__ == '__main__':
    print(main())
